# base configuration {{{
set -x EDITOR nvim
set -x PAGER less
set -x BROWSER open
type -q fish && set -x SHELL (which fish)
set -x LANG en_US.UTF-8
set -x LC_ALL en_US.UTF-8
#}}}
# Workspace {{{
set -x WORKSPACE_PATH $HOME/workspace
set -x PROJECTS_PATH $WORKSPACE_PATH/projects
set -x LABO_PATH $WORKSPACE_PATH/labo
set -x REPO_PATH $WORKSPACE_PATH/github
set -x PATH $HOME/.yarn/bin $HOME/.config/yarn/global/node_modules/.bin $HOME/.local/bin $PATH
# }}}
# Configuration fisher path {{{
set -g fisher_path $HOME/.local/share/fish/bundle

set fish_function_path $fish_function_path[1] $fisher_path/functions $fish_function_path[2..-1]
set fish_complete_path $fish_complete_path[1] $fisher_path/completions $fish_complete_path[2..-1]

for file in $fisher_path/conf.d/*.fish
  builtin source $file 2> /dev/null
end
#}}}
# rbenv {{{
type -q rbenv && source (rbenv init -|psub)
#}}}
# nodenv {{{
type -q nodenv && source (nodenv init -|psub)
#}}}
# pyenv {{{
type -q pyenv && source (pyenv init -|psub)
#}}}
# direnv {{{
type -q direnv && direnv hook fish | source
#}}}
# asdf {{{
if test -e ~/.asdf/asdf.fish
  source ~/.asdf/asdf.fish
end
#}}}
# linuxbrew {{{
if test -e /home/linuxbrew/.linuxbrew/bin/brew
  /home/linuxbrew/.linuxbrew/bin/brew shellenv | source
end
#}}}
# UA inspector {{{
set -x UA_INSPECTOR_DATABASE_PATH '/Users/raw1z/workspace/lib/ua_inspector'
#}}}
# Git {{{
set -x GIT_MERGE_AUTOEDIT no
#}}}
# Elixir {{{
set -x ERL_AFLAGS "-kernel shell_history enabled"
#}}}
# Docker {{{
set -x DOCKER_ID_USER "raw1z"
#}}}
# Source the aliases {{{
test -f $HOME/.config/fish/aliases.fish; and . $HOME/.config/fish/aliases.fish

# load the projects shell aliases
for x in $HOME/.config/project-aliases/*.fish
  source $x
end
#}}}
# Source the local config {{{
test -f $HOME/.config/fish/local_config.fish; and . $HOME/.config/fish/local_config.fish
#}}}
# Base16 Shell {{{
if status --is-interactive
  set BASE16_SHELL "$HOME/.config/base16-shell"
  source "$BASE16_SHELL/profile_helper.fish"
end
#}}}
