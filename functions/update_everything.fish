function __update_system_part
    argparse 't/title=' 'p/progress=' -- $argv
    or return

    set title $_flag_title
    set progress $_flag_progress
    if test -z "$progress"
        set progress $title
    end
    set command $argv
    set checkmark (gum style --foreground "#00bb00" "✓")

    gum spin \
        --title="$progress..." \
        --show-error \
        -- $command; and gum log "$checkmark $title"
end

function update_everything
    argparse with-tmux -- $argv
    or return

    # fish plugins {{{
    __update_system_part -t "fish plugins" \
        -- fish -c 'fisher update'
    # }}}
    # carapace {{{
    __update_system_part -t carapace \
        -- fish -c update_carapace
    # }}}
    # tldr database {{{
    __update_system_part -t "tldr database" \
        -- tldr --update
    # }}}
    # tmux {{{
    if set --query _flag_with_tmux
        __update_system_part -t tmux -- fish -c update_tmux
        if not test $status -eq 0
            __update_system_part -t tmux \
                -p "tmux incremental update failed, rebuilding..." \
                -- fish -c "update_tmux --rebuild"
        end
    end
    __update_system_part -t "tmux plugins" \
        -- ~/.tmux/plugins/tpm/bin/update_plugins all
    # }}}
    # colors {{{
    __update_system_part -t colors -- fish -c update_colors
    # }}}
    # asdf {{{
    __update_system_part -t "asdf plugins" \
        -- asdf plugin update --all
    # }}}
    # brew {{{
    __update_system_part -t "brew update" -- brew update
    __update_system_part -t "brew upgrade" -- brew upgrade
    # }}}
    # neovim {{{
    __update_system_part -t neovim -- fish -c update_nvim
    if not test $status -eq 0
        __update_system_part -t neovim \
            -p "neovim incremental update failed, rebuilding..." \
            -- fish -c "update_nvim --rebuild"
    end

    __update_system_part -t "neovim plugins" \
        -- fish -c update_nvim_plugins
    # }}}
    # ollama {{{
    __update_system_part -t ollama \
        -- fish -c update_ollama
    # }}}
end
