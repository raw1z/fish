function labo
    set name $argv[1]
    set directory $argv[2]

    if test -z "$name"
        echo "usage: labo <name> [directory]"
        return 1
    end

    if test -z "$directory"
        set directory $LABO_PATH/$name
    end

    mkdir -p $directory

    if not tmux has-session -t $name
        env TMUX="" tmux new-session -s $name -c $directory -d
        if test -z "$directory/.envrc"
            tmux new-window -n editor -c $directory -t $name:2 nvim
        else
            tmux new-window -n editor -c $directory -t $name:2 direnv exec $directory nvim
        end
    end

    if test -z "$TMUX"
        tmux attach -t $name
    else
        tmux switch -t $name
    end
end
