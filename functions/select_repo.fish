function __repo_from_stdin
    read -l -z name
    set sanitized_name (string trim $name)
    if not test -z "$sanitized_name"
        set repo_folder "$REPO_PATH/$sanitized_name"
        if test -d "$repo_folder"
            tmux new-window -n $sanitized_name -c $repo_folder
        end
    end
end

function select_repo
    /bin/ls -l -1 $REPO_PATH | filter | __repo_from_stdin
end
