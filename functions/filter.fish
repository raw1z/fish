function filter
    argparse 's/selected=' 'p/preview=' 'size=' -- $argv
    or return

    read --null --list --delimiter=\n options

    # use ctrl-j and ctrl-k to navigate the list
    set bind_spec "ctrl-j:down"
    set bind_spec "$bind_spec,ctrl-k:up"

    # use ctrl-u to go to the top of the list
    set bind_spec "$bind_spec,ctrl-u:pos(0)"

    # on change, move the cursor to the top of the list
    set bind_spec "$bind_spec,change:pos(0)"

    if set --query _flag_selected
        # on start, set the cursor to the position of the selection
        set selection_index (contains --index $_flag_selected $options)
        set bind_spec "$bind_spec,start:pos($selection_index)"
    end

    if set --query _flag_preview
        set bind_spec "$bind_spec,ctrl-p:execute-silent($_flag_preview)"
    end

    set size "50%"
    if set --query _flag_size
        set size "$_flag_size%"
    end

    string join \n $options \
        | grep -v '^\s*$' \
        | fzf --sync \
        -d \n \
        --tmux "$size" \
        --padding 1% \
        --print-query \
        --bind "$bind_spec" \
        | tail -1
end
