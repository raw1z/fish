function select_man_page
    set input (apropos . 2>/dev/null | sed -r 's#([^\(]+)\(.+\).+#\1#g' | filter)
    if not test -z "$input"
        new_window --title="man" -- "man $input"
    end
end
