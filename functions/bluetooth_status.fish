function bluetooth_status
    set powered (blueutil --power)
    set connected (blueutil --connected | awk -F '"' '{ print $2 }')

    jq -n --arg powered "$powered" --arg connected "$connected" '{$powered, $connected}'
end
