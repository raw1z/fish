function switch_theme
    set current_theme (cat ~/.current_theme)
    wsctl list-themes | filter -s "$current_theme" -p "wsctl theme {}" | wsctl theme
end
