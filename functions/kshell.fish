function kshell
    argparse "i/image=" -- $argv

    set command $argv

    if set --query _flag_image
        set image $_flag_image
    else
        set image busybox
    end

    if test -z "$command"
        set command sh
    end

    set name kshell-(openssl rand -hex 4)

    kubectl run --rm -i --tty $name --image=$image -- $command
end
