function read_kube_secret -d "return the content of a k8s secret"
    set secret_name (string split "/" $argv[1])
    set secret_key $argv[2]
    set encoded_value (kubectl get -n $secret_name[1] secrets/$secret_name[2] --template="{{index .data \"$secret_key\"}}")
    echo -n $encoded_value | base64 -d
end
