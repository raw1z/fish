function dot_edit
    set session_name dot
    set tab_id $argv[1]

    if tmux has-session -t $session_name
        if test -z "$TMUX"
            tmux attach -t $session_name
        else
            tmux switch -t $session_name:"$tab_id"
        end
    else
        env TMUX="" tmux new-session -n dotfiles -s $session_name -c $HOME/.config/wsctl -d e
        tmux new-window -n hammerspoon -t $session_name:2 edit_hammerspoon
        tmux new-window -n fish -t $session_name:3 edit_fish
        tmux new-window -n neovim -t $session_name:4 edit_neovim
        tmux new-window -n projects -t $session_name:5 edit_projects
        tmux new-window -n ansible -t $session_name:6 edit_ansible
        tmux new-window -n sketchybar -t $session_name:7 edit_sketchybar

        if test -z "$TMUX"
            tmux attach -t $session_name:1
        else
            tmux switch -t $session_name:1
        end
    end
end
