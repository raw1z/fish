function docker_cleanup
    docker system prune --all --force --volumes
end
