function switch_session
    # get the name of the currently active tmux session
    set current_session (tmux display-message -p '#S')

    # show a session selector and save the selection
    set selected_session ( tmux list-sessions | awk -F':' 'NF { split($0, a, ":"); print a[1] }' | filter --selected "$current_session" --size 20 )

    # switch tmux session if the selected session is different from the current one
    if test "$selected_session" != "$current_session" -a "$selected_session" != ""
        tmux switch-client -t "$selected_session"
    end
end
