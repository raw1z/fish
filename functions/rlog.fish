function rlog
    set message $argv[1..]
    syslog -s -l notice "[rzlog] $message"
end
