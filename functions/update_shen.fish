function update_shen
    set shen_version $argv[1]
    if test -z "$shen_version"
        echo "usage: update_shen <version>"
        return
    end

    set install_dir /tmp/shen-install
    mkdir $install_dir

    set folder_name "shen-scheme-v$shen_version-macOS-bin"
    pushd $install_dir
    wget "https://www.shenlanguage.org/Download/$shen_version.zip"
    unzip "$shen_version.zip"
    cd $shen_version
    ros run -- --eval '(load "install.lsp")'
    cp -v sbcl-shen.exe ~/.local/bin/shen
    popd

    rm -r $install_dir
end
