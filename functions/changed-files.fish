function changed-files
    argparse p/pr d/diff -- $argv

    set base origin/main
    if set --query _flag_pr
        set -x GH_PAGER ""
        if set --query _flag_diff
            gh pr diff
        else
            gh pr diff --name-only
        end
    else
        if set --query _flag_diff
            git --no-pager diff --patience $base
        else
            git diff --name-status $base
        end
    end

end
