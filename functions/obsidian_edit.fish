function obsidian_edit
    set session_name obsidian

    if tmux has-session -t $session_name
        if test -z "$TMUX"
            tmux attach -t $session_name
        else
            tmux switch -t $session_name
        end
    else
        set folder ~/Library/Mobile\ Documents/iCloud~md~obsidian/Documents/Second\ Brain/
        env TMUX="" tmux new-session -n $session_name -s $session_name -c $folder -d e

        if test -z "$TMUX"
            tmux attach -t $session_name
        else
            tmux switch -t $session_name
        end
    end
end
