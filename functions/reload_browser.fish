function reload_browser
    set usage "reload-browser [--activate] [browser name]"
    set app $argv[2]

    if test -z "$app"
        set app "Firefox Developer Edition"
    end

    getopts $argv | read -l key value
    switch $key
        case activate
            /usr/bin/osascript -e "\
        tell application \"$app\"
          activate
        end tell
        tell application \"System Events\" to keystroke \"r\" using {command down}"
        case help
            echo $usage
        case '*'
            /usr/bin/osascript -e "\
        set prev to (path to frontmost application as text)
        tell application \"$app\"
          activate
        end tell
        delay 0.5
        tell application \"System Events\" to keystroke \"r\" using {command down}
        delay 0.5
        activate application prev"
    end
end
