function session-menu
    set var_content (tmux display-message -p "#{session_name}_menu" | xargs tmux showenv -g)
    if not test -z "$var_content"
        tmux display-message -p "#{session_name}_menu" \
            | xargs tmux showenv -g \
            | awk -F'=' '{print $2}' \
            | xargs tmux display-menu
    end
end
