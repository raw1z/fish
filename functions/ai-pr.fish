function __ai_pr
    set pr_id $argv[1]
    if not test -z "$pr_id"
        gh pr checkout --quiet $pr_id
    end

    set temp_file (mktemp)
    echo "/run fish -c 'changed-files --pr --diff'" >$temp_file
    cat $temp_file

    set title (gh pr view $pr_id --json title --jq '.title' 2>/dev/null || echo "AI PR")
    new_window --title "$title" -- aider --vim --model sonnet --no-auto-commit --no-gitignore --yes-always --load $temp_file
end

function ai-pr
    set pr_id (tmux command-prompt -p "PR id:" "run-shell 'fish -c \"echo %%\"'")
    __ai_pr $pr_id
end
