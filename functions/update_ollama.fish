function update_ollama -d "update ollama all/specified models"
    argparse 'm/model=' -- $argv
    or return

    if set --query _flag_model
        set model $_flag_model
        echo Pulling model: "$model"
        ollama pull $model
        return
    end

    ollama ls | awk 'NR>1 {print $1}' | xargs -I{} fish -c 'update_ollama --model {}'
end
