function update_code-maat
    set app_dir "$REPO_PATH/code-maat"
    if test -d $app_dir
        pushd $app_dir
        git pull
        lein uberjar
        popd
    else
        pushd $REPO_PATH
        git clone https://github.com/adamtornhill/code-maat.git
        cd code-maat
        lein uberjar
        popd
    end
end
