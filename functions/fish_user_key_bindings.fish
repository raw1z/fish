function __raw1z_insert_char #{{{
  commandline -i $argv
  commandline -f repaint
end #}}}
function __raw1z_insert_gt #{{{
  __raw1z_insert_char '>'
end #}}}
function __raw1z_insert_lt #{{{
  __raw1z_insert_char '<'
end #}}}
function __raw1z_insert_tilde #{{{
  __raw1z_insert_char '~'
end #}}}
function __raw1z_insert_pipe #{{{
  __raw1z_insert_char '|'
end #}}}
function __raw1z_insert_square_bracket_left #{{{
  __raw1z_insert_char '['
end #}}}
function __raw1z_insert_square_bracket_right #{{{
  __raw1z_insert_char ']'
end #}}}
function __raw1z_insert_brace_left #{{{
  __raw1z_insert_char '{'
end #}}}
function __raw1z_insert_brace_right #{{{
  __raw1z_insert_char '}'
end #}}}
function __raw1z_insert_backslash #{{{
  __raw1z_insert_char "\\"
end #}}}
function __raw1z_enable_vi_mode --description "Vi-style bindings that inherit emacs-style bindings in all modes" #{{{
  for mode in default insert visual
    fish_default_key_bindings -M $mode
  end
  fish_vi_key_bindings --no-erase
end
#}}}
function fish_user_key_bindings #{{{
  # enable vi-mode{{{
  __raw1z_enable_vi_mode
  #}}}
  # map kj sequence to escape {{{
  bind -M insert -m default kj backward-char force-repaint
  #}}}
  # maps in vi-mode {{{
  for mode in insert default visual
    bind -M $mode \ck forward-word
    bind -M $mode \cj accept-autosuggestion
    bind -M $mode \cr 'fzy_select_history (commandline -b)'
  end
  #}}}
  # easily add special characters {{{
  bind -M insert "çq" __raw1z_insert_gt
  bind -M insert "ççq" __raw1z_insert_lt
  bind -M insert "çn" __raw1z_insert_tilde
  bind -M insert "ç!" __raw1z_insert_pipe
  bind -M insert "ç(" __raw1z_insert_square_bracket_left
  bind -M insert "ç)" __raw1z_insert_square_bracket_right
  bind -M insert "çç(" __raw1z_insert_brace_left
  bind -M insert "çç)" __raw1z_insert_brace_right
  bind -M insert "ç/" __raw1z_insert_backslash
  #}}}
end #}}}
