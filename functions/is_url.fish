function is_url -d "Check if a string is a valid URL"
    set url $argv[1]
    if string match --quiet -r '^(https?://|ftp://|www\.)[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}(/.*)?' $url
        return 0
    else
        return 1
    end
end
