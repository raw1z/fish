function __labo_from_stdin
    read -l -z name
    set sanitized_name (string trim $name)
    if not test -z "$sanitized_name"
        labo $sanitized_name
    end
end

function select_labo
    /bin/ls -l -1 $LABO_PATH | filter | __labo_from_stdin
end
