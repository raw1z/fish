function new_window
    argparse 't/title=' k/keep-alive -- $argv
    or return

    set command "$argv"
    if set --query _flag_keep_alive
        set command "$command; and echo -n 'Press any key to close...'; and read -p '' -n 1 -s"
    end

    set title "$command"
    if set --query _flag_title
        set title $_flag_title
    end

    tmux new-window -n "$title" "$command"
end
