function repo_clone
    set url $argv[1]

    if test -z "$url"
        set url (pbpaste)
    end

    set folder (echo $url | sed -E 's/.+\/(.+)\.git/\1/g''')
    cd $REPO_PATH
    git clone $url
    cd $folder
    fish
end
