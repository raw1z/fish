function is_git_url -d "Check if a string is a valid git URL"
    set url $argv[1]

    # Check if it's a valid URL
    if not is_url $url
        return 1 # Not a valid URL
    end

    # Check if it ends with .git
    if string match --quiet -r '\.git$' $url
        return 0 # Valid Git repository URL
    else
        return 1 # Invalid Git repository URL
    end
end
