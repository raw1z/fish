function music_status
    set state (osascript -e 'tell application "Music" to get player state')
    if not test $state = playing
        jq -n --arg state "$state" '{$state}'
        return
    end

    set track (osascript -e 'tell application "Music" to get name of current track')
    set artist (osascript -e 'tell application "Music" to get artist of current track')
    set album (osascript -e 'tell application "Music" to get album of current track')
    set shuffle (osascript -e 'tell application "Music" to get the shuffle enabled')
    set repeat (osascript -e 'tell application "Music" to get the song repeat')

    jq -n \
        --arg state "$state" \
        --arg track "$track" \
        --arg artist "$artist" \
        --arg album "$album" \
        --arg shuffle "$shuffle" \
        --arg repeat "$repeat" \
        '{$state, $track, $artist, $album, $shuffle, $repeat}'
end
