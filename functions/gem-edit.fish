function gem-edit
    if not test -f Gemfile
        echo "can't find any Gemfile"
        return
    end

    set gem_name (cat Gemfile.lock \
                    | grep -E ".+ \(.+\)" \
                    | sed -r "s/\s*([a-z0-9_\-]+).*/\1/g" \
                    | string trim \
                    | sort \
                    | uniq \
                    | filter)

    set gem_path (bundle show $gem_name)
    pushd $gem_path
    new_window -t "$gem_name" -- nvim +Oil
    popd
end
