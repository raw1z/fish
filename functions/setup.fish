function setup
    set tags (string join , $argv)
    set working_folder $REPO_PATH/ansible-raw1z
    set -x DIRENV_LOG_FORMAT ""
    pushd $working_folder
    direnv exec $working_folder invoke run --tags=$tags
    popd
end
