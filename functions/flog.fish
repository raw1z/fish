function flog
    argparse w/watch t/truncate -- $argv

    set log_folder ~/.local/share/flog
    mkdir -p $log_folder

    set log_file "$log_folder/log.txt"

    if set --query _flag_watch
        if ! test -z "$log_file"
            touch $log_file
        end
        tail -f $log_file
    else if set --query _flag_truncate
        rm $log_file
    else
        set message $argv[1..]
        set current_date (date +%c)
        echo "----[$current_date]------------------------------------------------" >>$log_file
        echo "$message" >>$log_file
        echo "" >>$log_file
    end
end
