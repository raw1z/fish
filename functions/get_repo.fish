function get_repo
    set url (pbpaste)
    if not is_git_url "$url"
        set url (tmux command-prompt -p "git url:" "run-shell 'fish -c \"echo %%\"'")
    end

    set folder (echo $url | sed -E 's/.+\/(.+)\.git/\1/g''')
    set command "fish -c repo_clone"
    tmux new-window -n "$folder" -c "$REPO_PATH" "$command"
end
