function session
    set name $argv[1]
    pushd $HOME/.config/project-aliases

    set project_folder $PROJECTS_PATH/$name
    if test ! -d $project_folder
        echo "Project folder '$project_folder' doesn't exist"
        return
    end

    set project_file $name.fnl
    if test -e $project_file
        fennel $project_file
    else
        fennel default.fnl $name
    end

    popd
end
