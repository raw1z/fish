function ai-chat
    set folder (tmux display-message -p "#{pane_current_path}")
    set -l context (pretty_path "$folder")
    new_window --title "AI Chat: $context" -- "cd $folder && aider --vim --model sonnet --no-auto-commit --no-gitignore --code-theme=gruvbox-dark"
end
