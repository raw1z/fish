function update_lein
    pushd /tmp
    wget https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein
    set location $HOME/.local/bin/lein
    mv lein $location
    chmod a+x $location
    popd
end
