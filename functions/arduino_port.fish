function arduino_port
    set fqbn $argv[1]
    if test -z "$fqbn"
        echo "usage: arduino_port <fqbn>"
        return 1
    end

    arduino_boards | grep $fqbn | awk '{split($0,a," ");print a[1]}'
end
