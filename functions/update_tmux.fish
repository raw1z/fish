function __configure_tmux
    ./configure --prefix=/Users/raw1z/.local --enable-jemalloc --enable-utf8proc
end

function update_tmux
    argparse r/rebuild -- $argv

    set tmux_path $REPO_PATH/tmux
    set -x PKG_CONFIG_PATH /usr/local/Cellar/jemalloc/5.3.0/lib/pkgconfig/ /usr/local/Cellar/utf8proc/2.9.0/lib/pkgconfig/

    if not test -d "$tmux_path"
        git clone https://github.com/tmux/tmux.git $tmux_path
        pushd $tmux_path
        __configure_tmux
    else
        pushd $tmux_path

        if set --query _flag_rebuild
            make distclean && __configure_tmux
        end

        git fetch --tags --force
        git pull
    end

    make && make install
    popd
end
