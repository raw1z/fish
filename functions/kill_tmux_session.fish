function kill_tmux_session
    set session_count (tmux list-sessions | wc -l | sed "s/ //g")
    set current_session (tmux display -p "#{client_session}")
    set last_session (tmux display -p "#{client_last_session}")

    if test $session_count -eq 1
        tmux kill-session -t $current_session
    else if test -n "$last_session"
        tmux switch -l
    else
        tmux switch -n
    end

    tmux kill-session -t $current_session
end
