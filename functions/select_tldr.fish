function select_tldr
    set input (tldr -l | sort | uniq -u | grep -v '^$' | grep -v 'Pages' | filter)
    if not test -z "$input"
        new_window --keep-alive --title="tldr" -- "tldr $input"
    end
end
