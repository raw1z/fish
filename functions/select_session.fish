function __session_from_stdin
    read -l -z name
    set sanitized_name (string trim $name)
    if not test -z "$sanitized_name"
        session $sanitized_name
    end
end

function select_session
    /bin/ls -l -1 $PROJECTS_PATH | filter | __session_from_stdin
end
