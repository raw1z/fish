function update_colors
    set schemes_path $REPO_PATH/iTerm2-Color-Schemes
    if not test -d "$schemes_path"
        git clone https://github.com/mbadolato/iTerm2-Color-Schemes.git $schemes_path
    end

    pushd $schemes_path
    git pull
    set template "
name: NAME
author: unknown
dark: {{ Dark_Theme }}
palette:
  base00: \"#{{ Ansi_0_Color.hex }}\" # black
  base01: \"#{{ Ansi_1_Color.hex }}\" # red
  base02: \"#{{ Ansi_2_Color.hex }}\" # green
  base03: \"#{{ Ansi_3_Color.hex }}\" # yellow
  base04: \"#{{ Ansi_4_Color.hex }}\" # blue
  base05: \"#{{ Ansi_5_Color.hex }}\" # magenta
  base06: \"#{{ Ansi_6_Color.hex }}\" # cyan
  base07: \"#{{ Ansi_7_Color.hex }}\" # white
  base08: \"#{{ Ansi_8_Color.hex }}\" # bright black
  base09: \"#{{ Ansi_9_Color.hex }}\" # bright red
  base0A: \"#{{ Ansi_10_Color.hex }}\" # bright green
  base0B: \"#{{ Ansi_11_Color.hex }}\" # bright yellow
  base0C: \"#{{ Ansi_12_Color.hex }}\" # bright blue
  base0D: \"#{{ Ansi_13_Color.hex }}\" # bright magenta
  base0E: \"#{{ Ansi_14_Color.hex }}\" # bright cyan
  base0F: \"#{{ Ansi_15_Color.hex }}\" # bright white
named_colors:
  foreground: \"#{{ Foreground_Color.hex }}\"
  background: \"#{{ Background_Color.hex }}\"
  cursor: \"#{{ Cursor_Color.hex }}\"
  cursor_text: \"#{{ Cursor_Text_Color.hex }}\"
  selection: \"#{{ Selection_Color.hex }}\"
  selected_text: \"#{{ Selected_Text_Color.hex }}\"
    "

    # generate the scheme files
    echo $template >tools/templates/wsctl.yaml
    rm -rf wsctl
    mkdir wsctl
    cd tools
    ./gen.py -t wsctl

    # sanitize scheme files filenames
    cd ../wsctl
    for filename in (ls -1 *.yaml)
        set sanitized_filename (string replace -a "'" "" $filename)
        set scheme_name (
            string replace -a "'" "" $filename \
                | string replace ".yaml" ""
        )
        gsed -i -e "s/NAME/$scheme_name/g" ./$sanitized_filename
    end
    popd

    set colors_path $HOME/.config/wsctl/colors/base
    rm $colors_path
    ln -s $schemes_path/wsctl $colors_path
end
