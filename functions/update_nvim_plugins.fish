function update_nvim_plugins
    set vim_path $HOME/.config/nvim

    pushd $vim_path

    # check if there are any changes
    git diff --exit-code --name-only
    set has_changes $status

    if test $has_changes -eq 1
        git stash -u
    end

    # update plugins
    nvim --headless +PluginsHeadlessUpdate

    # check if any update was made
    git diff --exit-code --name-only
    if test $status -eq 1
        # commit lazy-lock.json
        git add lazy-lock.json
        git commit -m "chore(plugins): update"
    end

    if test $has_changes -eq 1
        git stash pop
    end

    popd
end
