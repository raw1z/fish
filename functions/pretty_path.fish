function pretty_path --description "Format paths with icons for better readability"
    # Store the input path
    set input_path $argv[1]
    if test -z "$input_path"
        set input_path $PWD
    end

    # Replace known path prefixes with icons
    set output (
      echo $input_path \
        | string replace "$PROJECTS_PATH/" "􀈕  " \
        | string replace "$LABO_PATH/" "􁂶  " \
        | string replace "$REPO_PATH/" "􀤅  " \
        | string replace "$HOME/.config/" "􀍟  " \
        | string replace "$HOME/" "􀎟  "
    )

    echo $output
end
