function tree --wraps=ls
    eza --tree --icons=always $argv
end
