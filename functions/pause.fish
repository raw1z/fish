function pause
    set message $argv
    if not test -z "$message"
        echo $message
    end
    read -p '' -n 1 -s
end
