function inspect_port
    argparse k/kill -- $argv
    or return

    set port $argv[1]

    set line (lsof -nP -i4TCP:$port | grep LISTEN)
    if test -z "$line"
        echo "can't find any running app listening on port $port"
        return
    end

    set pid (echo "$line" | awk '{print $2}')
    ps -p $pid -o pid,comm
    if set --query _flag_kill
        kill -9 $pid
        echo "killed proc $pid"
    end
end
