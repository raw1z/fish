function new_python_env
    set env_name $argv[1]

    if test -z "$env_name"
        set env_name ".env"
    end

    # if the virtualenv folder already exists
    # then ask if it should be destroyed and recreated
    if test -d "$env_name"
        read -l -P "[$env_name] already exists. Destroy and recreate? [y/N] " confirm
        if test "$confirm" = y -o "$confirm" = Y
            echo "[$env_name] removing existing environment"
            rm -rf "$env_name"
        else
            echo "[$env_name] keeping existing environment"
            return 0
        end
    end

    # create the virtual env
    echo "[$env_name] create virtual env"
    python -m venv $env_name

    # ensure that the virtual env is activated
    # when we enter the folder
    echo "[$env_name] configure and enabled direnv"
    echo "export VIRTUAL_ENV=\$PWD/$env_name" >.envrc
    echo "export PATH=\$PWD/$env_name/bin:\$PATH" >>.envrc
    direnv allow .

    # update pip
    echo "[$env_name] upgrade pip"
    pip install --upgrade pip
end
