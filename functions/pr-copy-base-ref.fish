function pr-copy-base-ref
    set base (gh pr view $arv --json baseRefOid | jq -r ".baseRefOid")
    if not test -z "$base"
        echo $base | pbcopy
        echo "copied base ref: $base"
    end
end
