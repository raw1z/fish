function network_status
    set ip_address (scutil --nwi | grep address | sed 's/.*://' | tr -d ' ' | head -1)
    set wifi_name (system_profiler SPAirPortDataType | awk '/Current Network Information:/ { getline; print substr($0, 13, (length($0) - 13)); exit }')
    set vpn (scutil --nwi | grep -m1 'utun' | awk '{ print $1 }')

    set tailscale_status (networksetup -showpppoestatus "Tailscale")
    if test "$tailscale_status" = connected
        set vpn Tailscale
    end

    set hotspot_status (networksetup -showpppoestatus "Hotspot Shield VPN (Hydra)")
    if test "$hotspot_status" = connected
        set vpn "Hotspot Shield VPN"
    end

    jq -n --arg ip "$ip_address" --arg wifi "$wifi_name" --arg vpn "$vpn" '{$ip, $wifi, $vpn}'
end
