function toggle_mic
    set current_volume (osascript -e "input volume of (get volume settings)")
    if test $current_volume = 0
        set volume 50
        osascript -e "set volume input volume $volume"
        sketchybar --trigger mic_toggled VOLUME=$volume
    else
        osascript -e "set volume input volume 0"
        sketchybar --trigger mic_toggled VOLUME=0
    end
end
