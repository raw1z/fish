function switch_font
    set current_font (cat ~/.current_font)
    wsctl list-fonts | filter -s "$current_font" -p "wsctl font {}" | wsctl font
end
