function __raw1z_git_local_branches
  command git for-each-ref --format='%(refname)' refs/heads/ refs/remotes/ 2>/dev/null \
    | string replace -rf "^refs/heads/$argv[1]/(.*)\$" '$1'
end
