function arduino_fqbns
    set boards (arduino_boards)
    for board in $boards
        echo $board | awk '{split($0,a," ");print a[2]}'
    end
end
