function ardio
    set fqbn $argv[1]
    if test -z "$fqbn"
        set boards (arduino_boards)
        set fqbn (echo $boards[1] | awk '{split($0,a," ");print a[2]}')
    end

    minicom -b 9600 -o -D (arduino_port $fqbn)
end
