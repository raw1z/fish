function reload_ghostty_conf
    argparse f/fonts -- $argv

    if set --query _flag_fonts
        osascript -e 'tell application "System Events" to keystroke "f" using {command down, shift down}'
    else
        osascript -e 'tell application "System Events" to keystroke "r" using {command down, shift down}'
    end
end
