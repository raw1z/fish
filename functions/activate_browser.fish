function activate_browser
    set app $argv[1]

    if test -z "$app"
        set app "Firefox Developer Edition"
    end

    /usr/bin/osascript -e "tell application \"$app\" to activate"
end
