function update_roc
    set roc_repo "$REPO_PATH/roc"

    if ! test -d "$roc_repo"
        git clone https://github.com/roc-lang/roc.git $roc_repo
    end

    set filter apple_silicon
    if test (uname -m) = x86_64
        set filter x86_64
    end

    pushd $roc_repo
    set query "select(.assets) | .[] | map(.url | select(. | contains(\"macos_$filter\")))"
    set download_url (gh release view --json=assets --jq="$query" nightly | jq -r -c ". | last")
    popd

    set install_dir /tmp/roc-install
    mkdir $install_dir

    pushd $install_dir
    set tar_name (string replace "https://github.com/roc-lang/roc/releases/download/nightly/" "" $download_url)
    wget --output-document $tar_name $download_url
    tar xkf $tar_name

    set roc_folder_name (eza -D -1 | head -1)
    cp $roc_folder_name/roc ~/.local/bin
    cp $roc_folder_name/roc_language_server ~/.local/bin
    cp $roc_folder_name/lib/* ~/.local/lib/
    popd

    rm -rf $install_dir
end
