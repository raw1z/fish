function mksand -d "create a sandbox folder and create a tmux window for it"
    set sandbox_name $argv[1]
    set sandbox_path (mktemp -d "/tmp/$sandbox_name.my-sandbox")
    if test -z "$sandbox_name"
        set sandbox_path (mktemp -d /tmp/my-sandbox.XXX)
    end
    tmux split-window -v -c "$sandbox_path"
end
