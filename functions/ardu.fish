function ardu
    set fqbn $argv[1]
    if test -z "$fqbn"
        set boards (arduino_boards)
        set fqbn (echo $boards[1] | awk '{split($0,a," ");print a[2]}')
    end

    arduino-cli upload -p (arduino_port $fqbn) --fqbn $fqbn
end
