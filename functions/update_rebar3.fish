function update_rebar3
    set rebar3_path ~/.local/bin/rebar3
    curl -o $rebar3_path https://s3.amazonaws.com/rebar3/rebar3
    chmod +x $rebar3_path
end
