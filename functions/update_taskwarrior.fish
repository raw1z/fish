function update_taskwarrior
    set taskwarrior_path $REPO_PATH/taskwarrior
    if not test -d "$taskwarrior_path"
        git clone https://github.com/taskwarrior/taskwarrior.git $taskwarrior_path
    end

    pushd $taskwarrior_path
    git stash -u
    git checkout stable
    git pull
    git stash pop
    set install_prefix ~/.local/
    cmake -S . -B build -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$install_prefix
    cmake --build build
    cmake --install build
    mv $install_prefix/bin/task $install_prefix/bin/taskw
    popd
end
