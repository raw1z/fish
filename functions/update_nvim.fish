function update_nvim
    argparse r/rebuild 'c/commit=' -- $argv

    set neovim_path $REPO_PATH/neovim
    if not test -d "$neovim_path"
        git clone https://github.com/neovim/neovim.git $neovim_path
    end

    pushd $neovim_path

    if set --query _flag_rebuild
        make distclean
    end

    if set --query _flag_commit
        git checkout $_flag_commit
    else
        git checkout master
    end

    git stash
    git fetch --tags --force
    git pull
    git stash pop
    make CMAKE_BUILD_TYPE=RelWithDebInfo
    make CMAKE_BUILD_TYPE=RelWithDebInfo CMAKE_INSTALL_PREFIX=$HOME/.local install
    popd
end
