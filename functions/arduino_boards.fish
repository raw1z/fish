function arduino_boards
    arduino-cli board list \
        | grep USB \
        | string replace "Serial Port (USB)" "" \
        | string replace "Arduino Mega or Mega 2560" "" \
        | string replace "Arduino Uno" "" \
        | string replace -r "\s+" " "
end
