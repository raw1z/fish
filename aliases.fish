# misc {{{
abbr -a e nvim
abbr -a sbcl rlwrap sbcl
abbr -a hk heroku
abbr -a edev easy_dev
alias shell_palette='bash $HOME/.config/fish/utils/shell_palette.sh'

function ls
  exa $argv
end

function swift
  set -x PATH /System/Library/Frameworks/Python.framework/Versions/Current/bin $PATH
  /usr/bin/swift $argv
end

function lldb
  set -x PATH /System/Library/Frameworks/Python.framework/Versions/Current/bin $PATH
  /usr/bin/lldb $argv
end

function chrome
  open -g -a "Google Chrome" $argv
end

function nb_cores
  sysctl -n hw.ncpu
end

function ios_simulator
  open /Applications/Xcode.app/Contents/Developer/Applications/Simulator.app
end

function android_simulator
  fish -c '$HOME/Library/Android/sdk/emulator/emulator -no-snapshot -avd PIXEL_API_22  -netdelay none -netspeed full 2> /dev/null' &
end

function list_puma_procs
  ps aux | grep puma | grep -v "grep"
end

function kill_puma_procs
  kill (list_puma_procs | awk '/ / {print $2}')
end

function update_nvim_providers
  python2 -m pip install --user --upgrade pynvim
  python3 -m pip install --user --upgrade pynvim
  gem install neovim --no-document
  yarn global add neovim
end
#}}}
# docker {{{
abbr -a dk docker
abbr -a dkps docker ps
abbr -a dke docker exec -it
abbr -a dkr docker run --rm -it
abbr -a dkrm docker rm --force

abbr -a dki docker image
abbr -a dkils docker image ls

function dksh
  docker exec -it $argv sh
end
#}}}
# docker-compose {{{
abbr -a dkc docker-compose
abbr -a dkcps docker-compose ps
abbr -a dkcr docker-compose run --rm
abbr -a dkce docker-compose exec
abbr -a dkcl docker-compose logs
abbr -a dkcu docker-compose up -d
abbr -a dkcd docker-compose down

abbr -a dkm docker-machine
#}}}
# kubernetes {{{
abbr -a kc kubectl
abbr -a kcg kubectl get
abbr -a kcd kubectl describe
abbr -a kcdel kubectl delete
abbr -a kcr kubectl run
abbr -a kcc kubectl create
abbr -a kccf kubectl create -f
abbr -a kca kubectl apply
abbr -a kcaf kubectl apply -f
abbr -a kcl kubectl logs
abbr -a kcpf kubectl port-forward
abbr -a kch kubectl help
abbr -a kce kubectl edit
abbr -a kccp kubectl cp

abbr -a kcga kubectl get all
abbr -a kcgn kubectl get nodes
abbr -a kcgd kubectl get deployment
abbr -a kcgs kubectl get services
abbr -a kcgp kubectl get pods
abbr -a kcgpa kubectl get pods --all-namespaces
abbr -a kcgev kubectl get events

abbr -a kccfg kubectl config
abbr -a kcexec kubectl exec -it
abbr -a kcapi kubectl api-resources
# }}}
# git {{{
abbr -a gst git status
abbr -a gco git checkout
abbr -a gd git diff
abbr -a gcod git checkout develop
abbr -a gcom git checkout master
abbr -a glog git log --oneline --decorate --no-merges
abbr -a gp git push
abbr -a gpm git push origin master:master
abbr -a gpl git pull
abbr -a grd git rebase develop

#}}}
# ruby {{{
abbr -a rkt rake --tasks --all
abbr -a be bundle exec
abbr -a dkrails docker-compose run --rm dev rails
abbr -a dkrake docker-compose run --rm dev rake
abbr -a rc rails console
abbr -a rdb rails dbconsole
#}}}
# brew {{{
abbr -a brupd "brew update && brew outdated"
abbr -a brupg brew upgrade
abbr -a brls brew list
abbr -a brsrv brew services
abbr -a brin brew install
abbr -a brun brew uninstall
# }}}
# ansible {{{
function ansible_vars
  ansible -m setup $argv
end
# }}}
# arduino {{{
function arduino_boards #{{{
  arduino-cli board list \
  | grep USB \
  | string replace "Serial Port (USB)" "" \
  | string replace "Arduino Mega or Mega 2560" "" \
  | string replace "Arduino Uno" "" \
  | string replace -r "\s+" " "
end #}}}
function arduino_fqbns #{{{
  set boards (arduino_boards)
  for board in $boards
    echo $board | awk '{split($0,a," ");print a[2]}'
  end
end #}}}
function arduino_port #{{{
  set fqbn $argv[1]
  if test -z "$fqbn"
    echo "usage: arduino_port <fqbn>"
    return 1
  end

  arduino_boards | grep $fqbn | awk '{split($0,a," ");print a[1]}'
end #}}}
function ardc #{{{
  set fqbn $argv[1]
  if test -z "$fqbn"
    set boards (arduino_boards)
    set fqbn (echo $boards[1] | awk '{split($0,a," ");print a[2]}')
  end

  arduino-cli compile --fqbn $fqbn
end #}}}
function ardu #{{{
  set fqbn $argv[1]
  if test -z "$fqbn"
    set boards (arduino_boards)
    set fqbn (echo $boards[1] | awk '{split($0,a," ");print a[2]}')
  end

  arduino-cli upload -p (arduino_port $fqbn) --fqbn $fqbn
end #}}}
function ardio
  set fqbn $argv[1]
  if test -z "$fqbn"
    set boards (arduino_boards)
    set fqbn (echo $boards[1] | awk '{split($0,a," ");print a[2]}')
  end

  minicom -b 9600 -o -D (arduino_port $fqbn)
end

complete -f -c ardc -n "not __fish_seen_subcommand_from (arduino_fqbns)" -a  "(arduino_fqbns)"
complete -f -c ardu -n "not __fish_seen_subcommand_from (arduino_fqbns)" -a  "(arduino_fqbns)"
# }}}
# workspace {{{
function fishedit #{{{
  pushd $HOME/.config/fish
  nvim
  popd
end
#}}}
function vimedit #{{{
  pushd $HOME/.config/nvim
  nvim
  popd
end
#}}}
function dotfilesedit #{{{
  pushd $HOME/workspace/github/dotfiles
  nvim
  popd
end
#}}}
function ansibledit #{{{
  pushd $HOME/workspace/github/ansible-raw1z
  nvim
  popd
end
#}}}
function projectsedit #{{{
  pushd $HOME/.config/project-aliases
  nvim
  popd
end
#}}}
function edit_workspace #{{{
  set session_name workspace

  if tmux has-session -t $session_name
    tmux attach -t $session_name
  else
    # fish
    tmux new-session -s $session_name -n fish -d "fishedit"
    tmux set-window-option remain-on-exit on

    # vim
    tmux new-window -n vim -t $session_name:2 "vimedit"
    tmux set-window-option remain-on-exit on

    # dotfiles
    tmux new-window -n dotfiles -t $session_name:3 "dotfilesedit"
    tmux set-window-option remain-on-exit on

    # ansible
    tmux new-window -n ansible -t $session_name:4 "ansibledit"
    tmux set-window-option remain-on-exit on

    # projects
    tmux new-window -n projects -t $session_name:5 "projectsedit"
    tmux set-window-option remain-on-exit on

    # shell
    tmux new-window -n shell -t $session_name:6
    tmux set-window-option remain-on-exit on

    tmux attach -t $session_name
  end
end
#}}}
# }}}
