set -x WORKSPACE_PATH $HOME/workspace
set -x PROJECTS_PATH $WORKSPACE_PATH/projects
set -x LABO_PATH $WORKSPACE_PATH/labo
set -x REPO_PATH $WORKSPACE_PATH/github

fish_add_path /usr/local/bin
fish_add_path $HOME/.config/yarn/global/node_modules/.bin
fish_add_path $HOME/.pub-cache/bin
fish_add_path /Applications/Postgres.app/Contents/Versions/latest/bin
fish_add_path $HOME/.krew/bin
fish_add_path /opt/local/bin/
fish_add_path $HOME/.cargo/bin
fish_add_path $HOME/.cabal/bin
