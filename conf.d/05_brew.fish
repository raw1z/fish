if type -q brew
    fish_add_path --append /opt/homebrew/bin
    set fish_complete_path $fish_complete_path /opt/homebrew/share/fish/vendor_completions.d
end
