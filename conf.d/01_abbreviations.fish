# misc {{{
abbr -a cdt cd /tmp
# }}}
# docker {{{
abbr -a dk docker
abbr -a dkb docker build
abbr -a dkl docker logs -f
abbr -a dkps docker ps
abbr -a dke docker exec -it
abbr -a dkr docker run --rm -it

abbr -a dki docker image
abbr -a dkils docker image ls
# }}}
# docker compose {{{
abbr -a dkc docker compose
abbr -a dkcps docker compose ps
abbr -a dkcr docker compose run --rm
abbr -a dkce docker compose exec
abbr -a dkcl docker compose logs
abbr -a dkcu docker compose up -d
abbr -a dkcd docker compose down
# }}}
# kubernetes {{{
abbr -a kc kubectl

abbr -a kcg kubectl get
abbr -a kcga kubectl get all
abbr -a kcgd kubectl get deployment
abbr -a kcgev kubectl get events
abbr -a kcgn kubectl get nodes
abbr -a kcgp kubectl get pods
abbr -a kcgpa kubectl get pods --all-namespaces
abbr -a kcgs kubectl get services

abbr -a kcd kubectl describe
abbr -a kcdd kubectl describe deployment
abbr -a kcdd kubectl describe nodes
abbr -a kcdn kubectl describe nodes
abbr -a kcdp kubectl describe pods
abbr -a kcds kubectl describe services

abbr -a kcdel kubectl delete
abbr -a kcr kubectl run
abbr -a kcc kubectl create
abbr -a kccf kubectl create -f
abbr -a kca kubectl apply -f
abbr -a kcl kubectl logs
abbr -a kcpf kubectl port-forward
abbr -a kcx kubectl explain
abbr -a kce kubectl edit
abbr -a kccp kubectl cp

abbr -a kccfg kubectl config
abbr -a kcexec kubectl exec -it
abbr -a kcapi kubectl api-resources
# }}}
# git {{{
abbr -a gb git branch
abbr -a gcl git clone
abbr -a gco git checkout
abbr -a gcob git checkout -b
abbr -a gcod git checkout develop
abbr -a gcom git checkout master
abbr -a gd git diff
abbr -a glog git log --oneline --decorate --no-merges
abbr -a gp git push
abbr -a gpf git push -f
abbr -a gpu git pull
abbr -a gra git rebase --abort
abbr -a grc git rebase --continue
abbr -a gst git status
# }}}
# ruby {{{
abbr -a bi "bundle check || bundle install"
abbr -a be bundle exec
abbr -a bl bundle list
abbr -a rt rails --tasks --all
#}}}
# brew {{{
abbr -a brupd "brew update && brew outdated"
abbr -a brupg brew upgrade
abbr -a brls brew list
abbr -a brs brew services
abbr -a brin brew install
abbr -a brun brew uninstall
# }}}
# redirect to /dev/null {{{
abbr -a --position anywhere ron "1> /dev/null"
abbr -a --position anywhere ren "2> /dev/null"
abbr -a --position anywhere ran "&> /dev/null"
# }}}
# taskwarrior {{{
abbr -a t taskw
abbr -a tl taskw list
abbr -a ta taskw add
abbr -a --set-cursor td taskw % done
abbr -a --set-cursor tx taskw % delete
abbr -a --set-cursor tm taskw % modify
abbr -a --set-cursor ti taskw % info
# }}}
# terraform {{{
abbr -a tf terraform
abbr -a tfp terraform plan
abbr -a tfa terraform apply -auto-approve
abbr -a tfv terraform validate
# }}}
# ollama {{{
abbr -a ol ollama
abbr -a oll ollama ls
abbr -a olp ollama pull
abbr -a olr ollama run
# }}}
