# software specific {{{
# docker {{{
set -x DOCKER_ID_USER raw1z
# }}}
# exa {{{
set -x EXA_ICON_SPACING 2
# }}}
# elixir {{{
set -x ERL_AFLAGS "-kernel shell_history enabled"
# }}}
# flutter {{{
fish_add_path --move $REPO_PATH/flutter/bin
# }}}
# git {{{
set -x GIT_MERGE_AUTOEDIT no
# }}}
# reader{{{
set -x READER_ACCESS_TOKEN IpuyIp8YpSBmXRFkXEntav73NapRPkQolZGSjQpbY1d3MOCQh4
# }}}
# aider {{{
set AIDER_CODE_THEME gruvbox-dark
# }}}
# }}}
# set maximum number of open files {{{
ulimit -S -n 200048
# }}}
# add $HOME/.local/bin to the path {{{
fish_add_path --global --move $HOME/.local/bin
# }}}
# add projects bin to the path {{{
fish_add_path $HOME/.config/project-aliases/bin
# }}}
