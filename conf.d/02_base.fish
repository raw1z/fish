set fish_greeting ""

set fish_cursor_default underscore
set fish_cursor_insert line blink
set fish_cursor_replace_one underscore
set fish_cursor_visual block

set -x EDITOR nvim
set -x PAGER less
set -x BROWSER open
type -q fish && set -x SHELL (which fish)
set -x LANG en_US.UTF-8
set -x LC_ALL en_US.UTF-8
set -x XDG_CONFIG_HOME ~/.config
