function __session_list_projects
    /bin/ls $PROJECTS_PATH
end

complete -f -c session -n "not __fish_seen_subcommand_from (__session_list_projects)" -a "(__session_list_projects)"
