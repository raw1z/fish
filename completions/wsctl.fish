function __wsctl_theme_list #{{{
    /bin/ls $HOME/.config/wsctl/colors/base/*.y*ml \
        | grep -v 256 \
        | xargs -n 1 basename \
        | string replace ".yml" "" \
        | string replace ".yaml" ""
end #}}}

set -l wsctl_subcommands "configure theme"
for subcommand in $wsctl_subcommands
    complete -f -c wsctl -n "not __fish_seen_subcommand_from $wsctl_subcommands" -a $subcommand
end
complete -f -c wsctl -n "__fish_seen_subcommand_from theme; and not __fish_seen_subcommand_from (__wsctl_theme_list)" -a "(__wsctl_theme_list)"
